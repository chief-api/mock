package boardmock

import (
	"net/http"

	"gitlab.com/chief-api/pkg/encodr"
)

// KeepAlive responds with a HTTP Status OK
func (mock *Board) KeepAlive(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		encodr.JSON(w, http.StatusOK, http.StatusText(http.StatusOK))
	default:
		encodr.JSONErr(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed))
	}
}
