package boardmock

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
)

// Salute sends the board info to the Chief API, via POST
func (mock *Board) Salute(chiefPort string) {
	jsonMock, err := json.Marshal(mock)
	if err != nil {
		logrus.Fatal(err)
	}

	salutePath := fmt.Sprintf("http://localhost:%s/salute", chiefPort)
	if _, err = http.Post(salutePath, appJSON, bytes.NewBuffer(jsonMock)); err != nil {
		logrus.Fatal(err)
	}
}
