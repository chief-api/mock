package boardmock

import (
	"fmt"
	"math/rand"
	"time"
)

// Init prepares the PrivateAddress, the device URL and the *rand.Rand for the mock
func (mock *Board) Init(mockPort string) {
	if mock.MAC == "" {
		mock.GenerateMAC()
	}
	if mock.IP == "" {
		mock.AllocateIP()
	}
	mock.PrivateAddress = fmt.Sprintf("/%s", mock.IP)

	fmtAddress := fmt.Sprintf("localhost:%s/%s", mockPort, mock.IP)
	mock.URL = fmt.Sprintf("http://%s", fmtAddress)

	mock.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
}
