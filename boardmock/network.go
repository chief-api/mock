package boardmock

import (
	"fmt"
	"log"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"
)

var (
	usedIPs  []string
	usedMACs []string
)

// AllocateIP generates a new IP based on the mock.Gateway
func (mock *Board) AllocateIP() {
	newip := newRandomIP(mock.Gateway)

Loop:
	for _, used := range usedIPs {
		if newip == used {
			newip = newRandomIP(mock.Gateway)
			continue Loop
		}
	}

	mock.IP = newip
}

// GenerateMAC generates a new random MAC address
func (mock *Board) GenerateMAC() {
	newmac := newRandomMAC()

Loop:
	for _, used := range usedMACs {
		if newmac == used {
			newmac = newRandomMAC()
			continue Loop
		}
	}

	mock.MAC = newmac
}

func newRandomIP(gw string) string {
	if ip := net.ParseIP(gw); ip == nil {
		log.Fatal("Invalid IP")
	}
	gwChunks := strings.Split(gw, ".")
	gwChunks[len(gwChunks)-1] = strconv.Itoa(randBetween(1, 255))

	return strings.Join(gwChunks, ".")
}

func newRandomMAC() string {
	return fmt.Sprintf(
		"%X-%X-%X-%X-%X-%X",
		randBetween(0, 255), randBetween(0, 255),
		randBetween(0, 255), randBetween(0, 255),
		randBetween(0, 255), randBetween(0, 255),
	)
}

func randBetween(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}
