package boardmock

import (
	"fmt"
	"net/http"

	"gitlab.com/chief-api/pkg/encodr"
)

// Read responds a HTTP GET with a mocked data
func (mock *Board) Read(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		random := mock.Rand.Float64()
		data := map[string]interface{}{
			"temperature": fmt.Sprintf("%.2f", random*20+20),
			"humidity":    fmt.Sprintf("%.2f%%", random*40+60),
		}

		encodr.JSON(w, http.StatusOK, data)
	default:
		encodr.JSONErr(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed))
	}
}
