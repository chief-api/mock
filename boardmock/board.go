package boardmock

import (
	"math/rand"

	"gitlab.com/chief-api/api/hardwr"
)

var (
	contentType = "Content-Type"
	appJSON     = "application/json; charset=utf-8"
)

// Board decorated a hardwr.Board with some additional fields
// so it can have some randomness of its own, etc.
type Board struct {
	*hardwr.Board
	PrivateAddress string     `json:"-"`
	Rand           *rand.Rand `json:"-"`
}
