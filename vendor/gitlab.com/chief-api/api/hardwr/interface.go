package hardwr

// NetInterface represents a board's ethernet interface
// which holds a IP, MAC address and Gateway
type NetInterface struct {
	IP      string `json:"ip"`
	Gateway string `json:"gateway"`
	MAC     string `json:"mac"`
}
