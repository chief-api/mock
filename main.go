package main

import (
	"net/http"
	"path/filepath"

	"github.com/justinas/alice"
	"github.com/sirupsen/logrus"
	"gitlab.com/chief-api/pkg/encodr"
	"gitlab.com/chief-api/pkg/loggr"
)

var (
	config = new(Config)
	err    error
)

// init runs only once, automatically, just before the execution reaching the main func
func init() {
	if config, err = readConfigFile(); err != nil {
		logrus.Fatal(err)
	}

	// For every device do:
	// Run Setup and then Salute to the Chief API
	for _, mock := range config.MockedBoards {
		mock.Init(config.MockPort)
		mock.Salute(config.ChiefPort)
	}
}

func main() {
	l := loggr.New()
	middlewrs := alice.New(l.Log)

	http.Handle("/", middlewrs.ThenFunc(ListMockedBoards))

	// Creating a new route for every mocked board
	for _, mock := range config.MockedBoards {
		http.HandleFunc(filepath.Join(mock.PrivateAddress, "keepalive"), mock.KeepAlive)

		http.Handle(filepath.Join(mock.PrivateAddress, "read"), middlewrs.ThenFunc(mock.Read))
	}

	// Starting server
	l.Logger.Infof("BitMock running at http://localhost:%s", config.MockPort)
	if err := http.ListenAndServe(":"+config.MockPort, nil); err != nil {
		l.Logger.Fatal(err)
	}
}

// ListMockedBoards lists all available devices within the mock server
func ListMockedBoards(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		encodr.JSON(w, http.StatusOK, config.MockedBoards)
	default:
		encodr.JSONErr(w, http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed))
		return
	}
}
