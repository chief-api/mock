package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"

	"gitlab.com/chief-api/mock/boardmock"
)

// Config represents the mock configuration file
type Config struct {
	ChiefPort    string             `json:"chief_port"`
	MockPort     string             `json:"mock_port"`
	MockedBoards []*boardmock.Board `json:"mocked_boards"`
}

// readConfigFile reads the JSON file passed to the mock server
// and unmarshals it into a *Config
func readConfigFile() (*Config, error) {
	// Check if the a file path was passed to the mock
	if len(os.Args) <= 1 {
		return nil, errors.New("A file path should be passed to the mock")
	}

	config := new(Config)
	filename := os.Args[1]

	readBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(readBytes, &config); err != nil {
		return nil, err
	}

	return config, nil
}
